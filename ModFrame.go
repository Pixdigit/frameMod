package frameMod

import (
	"github.com/pkg/errors"
	"github.com/veandco/go-sdl2/sdl"
	geo "gitlab.com/Pixdigit/geometry"
	"gitlab.com/Pixdigit/turboOcto"
)

type ModFrame struct {
	*turboOcto.Frame
	Rotation geo.Scalar
	FlipH    bool
	FlipV    bool
}

func NewModFrame(fr *turboOcto.Frame) *ModFrame {
	return &ModFrame{
		fr,
		0,
		false,
		false,
	}
}

func (mf *ModFrame) render() error {
	if !mf.Visible {
		return nil
	}

	center := mf.Pos()
	tl, tr, bl, br := mf.Corners()

	//Calculate new corners
	ntl := rotatePoint(tl, mf.Rotation, center)
	ntr := rotatePoint(tr, mf.Rotation, center)
	nbl := rotatePoint(bl, mf.Rotation, center)
	nbr := rotatePoint(br, mf.Rotation, center)

	//Get bounding rectangle
	left := findLowest([]geo.Scalar{ntl.X, ntr.X, nbl.X, nbr.X})
	top := findLowest([]geo.Scalar{ntl.Y, ntr.Y, nbl.Y, nbr.Y})
	right := findHighest([]geo.Scalar{ntl.X, ntr.X, nbl.X, nbr.X})
	bottom := findHighest([]geo.Scalar{ntl.Y, ntr.Y, nbl.Y, nbr.Y})

	flip := sdl.FLIP_NONE
	if mf.FlipV {
		flip |= sdl.FLIP_VERTICAL
	}
	if mf.FlipH {
		flip |= sdl.FLIP_HORIZONTAL
	}

	dstRect := &sdl.Rect{int32(left), int32(top), int32(right - left), int32(bottom - top)}
	sdlCenter := &sdl.Point{int32(center.X), int32(center.Y)}
	err := renderer.CopyEx(mf.Texture, nil, dstRect, float64(mf.Rotation), sdlCenter, flip);	if err != nil {return errors.Wrap(err, "could not copy Sprite frame to screenRenderer")}
	return nil
}

package frameMod

import (
	"fmt"
	"testing"

	"github.com/veandco/go-sdl2/sdl"
	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestTest(t *testing.T) {

	win, err := sdl.CreateWindow("title", sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED, 50, 50, sdl.WINDOW_SHOWN)
	if err != nil {
		tools.WrapErr(err, "could not create framework", t)
		t.FailNow()
	}
	rend, err := sdl.CreateRenderer(win, 0, sdl.RENDERER_SOFTWARE|sdl.RENDERER_PRESENTVSYNC)
	if err != nil {
		tools.WrapErr(err, "could not create framework", t)
		t.FailNow()
	}

	winSurf, err := win.GetSurface()
	if err != nil {
		tools.WrapErr(err, "could not create surface to render on", t)
	}

	Init(rend)

	texture, err := renderer.CreateTexture(sdl.PIXELFORMAT_RGBA8888, sdl.TEXTUREACCESS_STREAMING, 50, 50)
	if err != nil {
		tools.WrapErr(err, "could not create texture for testing", t)
	}

	sampleData := make([]byte, 50*50*4)
	for i := 0; i < 50*50*4; i += 4 {
		sampleData[i] = 255
		sampleData[i+1] = 128
		sampleData[i+2] = 0
		sampleData[i+3] = 255
	}

	texture.Update(nil, sampleData, 50*4)
	renderer.Copy(texture, nil, nil)

	surf, err := textureToSurface(texture)
	if err != nil {
		tools.WrapErr(err, "could not convert texture to surface", t)
	}

	// surf.Blit(nil, winSurf, nil)
	_ = winSurf

	rawData := *(*[]byte)(surf.Data())
	fmt.Println(rawData)

	sdl.Delay(1500)

}

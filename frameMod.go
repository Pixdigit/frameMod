package frameMod

import (
	"math"

	"github.com/veandco/go-sdl2/sdl"
	geo "gitlab.com/Pixdigit/geometry"
)

var renderer *sdl.Renderer

var rmask uint32 = 0x000000ff
var gmask uint32 = 0x0000ff00
var bmask uint32 = 0x00ff0000
var amask uint32 = 0xff000000

func init() {
	//Default is LIL_ENDIAN
	if sdl.BYTEORDER == sdl.BIG_ENDIAN {
		rmask = 0xff000000
		gmask = 0x00ff0000
		bmask = 0x0000ff00
		amask = 0x000000ff
	}
}

func Init(rend *sdl.Renderer) {
	renderer = rend
}

func rotatePoint(p geo.Point, angle geo.Scalar, center geo.Point) geo.Point {
	offset := geo.Point{0, 0}.Distance(center)
	pLocal := p
	pLocal.Sub(offset)

	cos := geo.Scalar(math.Cos(float64(angle)))
	sin := geo.Scalar(math.Sin(float64(angle)))

	rotatedP := geo.Point{
		X: pLocal.X*cos - pLocal.Y*sin,
		Y: pLocal.X*sin + pLocal.Y*cos,
	}

	rotatedP.Add(offset)

	return rotatedP
}

func findLowest(values []geo.Scalar) geo.Scalar {
    min := geo.Scalar(0)
	for _, v := range values {
		if v < min {
			min = v
		}
	}
	return min
}
func findHighest(values []geo.Scalar) geo.Scalar {
    max := geo.Scalar(0)
	for _, v := range values {
		if v < max {
			max = v
		}
	}
	return max
}
